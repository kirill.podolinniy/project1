
//Деструктуризация объекта

const firstUser = {
  firstName : 'Kirill',
  lastName : 'Podolinniy',
  age : '24',
  gender : 'male'
}
 let {firstName, lastName} = firstUser;
 //Присвоение новой переменной с деструктризацией.
 let{firstName : name} = firstUser;
 console.log(name);
//Деструктуризация и получение переменной, которую не содержит объект возвращает undefined
let{email} = firstUser;
console.log(email);

console.log(firstUser);
console.log(firstName);
console.log(lastName);



//Деструктуризация массива
const nuberString = [1, 2, 3, 4, 5, 6, 7, 8];
let [first, second, therth] = nuberString;
console.log(second);

function sayMyName ({
  firstName = 'Zell',
  lastName = 'Liew'
} = {}) { console.log(firstName + ' ' + lastName);
}

sayMyName();
