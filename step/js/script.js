const tabsTitle = document.querySelectorAll(".tabs li");
console.log(tabsTitle);
for (let i = 0; i < tabsTitle.length; i++) {
    tabsTitle[i].addEventListener('click', function (event) {
        console.log(event.target);
        const parentUl = this.closest(".tabs");
        for (let i = 0; i < parentUl.children.length; i++) {
            parentUl.children[i].classList.remove("active");
        }
        console.log(parentUl);

        this.classList.add("active");
        const tabsContent = document.querySelectorAll(".tabs-content div");
        console.log(tabsContent);
        for (let i = 0; i < tabsContent.length; i++) {
            tabsContent[i].classList.remove("active");
        };
        const tabId = this.dataset.target;
        console.log(tabId);

        const tab = document.getElementById(tabId);
        tab.classList.add("active");
        console.log(tab);


    });
};

$(document).ready(function () {

    $('.example-wrapper').isotope({
        itemSelector: '.example-box',
        layoutMode: 'fitRows'
    });

    $('.work-example  li').click(function () {
        $('.work-example li').removeClass('active');
        $(this).addClass('active');

        const selector = $(this).attr('data-filter');
        console.log(selector)
        $('.example-wrapper').isotope({
            filter: selector
        });
        return false;
    });
})



$('.example-box').hover(function (e) {
    // $(e.target).hide();
    const type = $(this);
    $(this).prepend(`<div class="hover-wrapper">
    <div class="hover-boxes">
        <div class="link-box">
                <i class="fa fa-link" aria-hidden="true"></i>
        </div>
        <div class="search-box">
                <i class="fas fa-search"></i>
        </div>
    </div>
    <p class="first-hover-text">creative design</p>
    <p class="second-hover-text">Web Design</p>
    </div>`);
}, function () {
    // $( this ).find("img").show();       
    $(this).find(".hover-wrapper").remove();
});

$('.slick-class-first').slick({
    dots: false,
    arrows: false,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: true,
    adaptiveWeight: true,
    asNavFor: '.slick-class-second'
});
$('.slick-class-second').slick({
    dots: false,
    slidesToScroll: 1,
    slidesToShow: 4,
    arrows: true,
    infinite: true,
    prevArrow: `<i class="myNawLeft fas fa-chevron-left slick-arrow" aria-hidden="true"></i>`,
    nextArrow: `<i class="myNawRight fas fa-chevron-right slick-arrow" aria-hidden="true"></i>`,
    speed: 300,
    asNavFor: '.slick-class-first',
    centerMode: true,
    focusOnSelect: true,
    adaptiveHeight: true,
    adaptiveWeight: true,

});
$(".example-box").hide().slice(0, 12).show();

function createNewImg() {
    $('#loadMoreButton').remove('#loadMoreButton');
    $('.preloader').css('margin-top','100px');
    $('.preloader').css('margin-bottom','100px');
    $('.preloader').css('display','block');
    setTimeout(getMoreImgs, 2000);
    function getMoreImgs(){
        $('.preloader').css('display','none');
        $(".example-box").show();

    }


}
$('#loadMoreButton').on('click',createNewImg);


