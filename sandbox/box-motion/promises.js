function applyForVisa(documents) {
    console.log('Обработка заявления...');
    let promise = new Promise(function (resolve, reject) {
        setTimeout(function () {
            Math.random() > .5 ? resolve({}) : reject('В визе отказано:не хватает документов');
        }, 1000);
    });
    return promise;
}

function getVisa(visa){
    console.info('Виза получена');
    return visa;
}

function bookHotel(visa) {
    console.log(visa);
    console.log('Бронируем отель');
}

function buyTickets() {
    console.log('Покупаем билеты');
}

applyForVisa({})
    .then(getVisa)
    .then(bookHotel)
    .then(buyTickets)
    .catch(error=>  console.error(error));
    