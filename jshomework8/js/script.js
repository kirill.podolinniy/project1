const container = document.getElementsByClassName("container")[0];
const priceField = document.getElementById("input-field");
priceField.addEventListener("focusout", function (event) {
    console.log(event.type);

    if (priceField.value <= 0 || priceField.value == null || priceField.value == " ") {
        const uncorrectValue = document.createElement("span");
        uncorrectValue.className = "span";
        uncorrectValue.textContent = "Please enter correct price.";
        container.append(uncorrectValue);
        priceField.style.color = ("red");
        priceField.style.borderColor = ("red");

    
    } else {
        const span = document.createElement("span");
        span.className = "span";
        span.textContent = `Текущая цена: ${priceField.value}`;
        container.prepend(span);
        const crossClose = document.createElement("a");
        crossClose.className = "cross";
        crossClose.textContent = "x";
        span.prepend(crossClose);
        priceField.classList.add("input-field-focusout");
        crossClose.addEventListener('click', function (event) {
            console.log(event.type);
            event.target.parentElement.remove();
            priceField.value = null;

        });
        priceField.addEventListener('click', function (e) {
            console.log(e.type);
            console.log(e.target);
            let spanTarget = document.querySelectorAll('span')[0];
            console.log(spanTarget);
            spanTarget.remove();

        });
    }

});


// priceField.addEventListener("focusin", function(event){
// const priceSpan = document.createElement("span");
// });
/*Пишем алгоритм
!1. Создать поле ввода imput с надписью price 
!*настроить input price
!2. Задать рамку зеленого при фокусе на поле ввода 
!3. Повесить обработчик событий фокуса поля ввода
!4. Задать функцию обработчика фокуса (на фокусе):
!- рамка зеленого цвета на поле ввода 
5. Задать функцию обработчика фокуса (потеря фокуса):
 !-создание span, который выврдит текст:  `Текущая цена: ${значение из поля ввода}`
 !-создание крестика
 !- окрашивание значени в поле ввода в зеленый цвет 
 !- удаление зеленой рамки на поле ввода
 - if  пользователь ввел число меньше 0:
 *подсветить поле красным 
 * создать <p>Please enter correct price.</p>

 
!6. Повесить обработчик событий "клик" на крестик span
!7. Задать функцию обработчика клика:
!- удаление span и крестика
!-обнуление поля ввода
*/